class Project < ActiveRecord::Base
  belongs_to :user
  has_many :issue
  # ensure that a user_id is present
  validates :user_id, presence: true

  # ensure that title is present and at least 10 chars long

  # ensure the url is present, and respects the URL format for http/https
end
