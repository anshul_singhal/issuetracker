class User < ActiveRecord::Base
	has_many :project
	has_many :issue
  	devise :database_authenticatable, :registerable, :rememberable, :validatable
end
