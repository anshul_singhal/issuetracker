json.array!(@issues) do |issue|
  json.extract! issue, :id, :title, :description, :privacy
  json.url issue_url(issue, format: :json)
end
