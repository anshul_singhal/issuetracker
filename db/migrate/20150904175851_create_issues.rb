class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.string :title
      t.text :description
      t.boolean :privacy

      t.timestamps null: false
    end
  end
end
